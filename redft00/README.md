This subfolder generates and deploys object files with one of the following signatures:

```
void r2r8(float * inout);
```
or
```
void r2r1(float * inout);
```

Remarks:
- The input and output size of the transforms is controlled by the makefile variable "size".
- The number 1 and 8 in the signature represent the number of independent 1D transforms that the function performs and it is controlled with the makefile variable "width".
- In the deployed object file, the function name changes according to the makefile variable "name".
- The transforms are performed in place.
- The target instruction set is AVX2.
- The data passed to r2r8 must have the AoS8 format.