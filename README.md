# genfft-kernels 

## Goal
The goal of this repository is to generate object files that can be in turn linked
against other, larger binaries. This repo manipulates FFTW's ```genfft``` output to achieve 
most compact signatures with no performance losses. This repo relies on ```objcopy``` 
to modify the function names within object files to mitigate possible symbol conflicts.

## Performance
On a Broadwell microarch, the object files generated with width=8 have shown to achieve
about 45% of the nominal FP32 performance (e.g. ```mpirun -n 32 --bind-to core:1 ./test-r2r8-65  | grep PEAK | awk '{s += $5; print s / 16.}'```).

## Example
Let's suppose you are developing code in ```~/mycode/``` and the repo is installed in ```~/genfft-kernels```.
From ```~/mycode/``` you could generate an 8-way SIMD 64-sized R2C DFT transform with: 
```
make -C ~/genfft-kernels/rdft rsize=64 name=mycode_r2c8_n64 sign=-1
```
The call will deploy the object file ```~/mycode/r2c8-64-fwd.o```. 
The object file will have just one function: ```mycode_r2c8_n64```.