#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>

#include <xmmintrin.h>

#include <math.h>
#include <fftw3.h>

enum 
{ 
    W = _W_, 
    N = _N_, 
    DIR = _DIR_, 
    WN = W * N,
    NH = N / 2,
    ntimes = 10000
};

#if _W_ == 8
#define _KERNEL_ c2c8
#else
#define _KERNEL_ c2c1
#endif

void _KERNEL_(float * rdata, float * idata);

void checkacc(
    const float * ref, 
    const float * res, 
    const int N, 
    const double tol)
{
    double linf_e = 0, l1_e = 0, 
	linf_ref = 0, l1_ref = 0;

    for(int i = 0; i < N; ++i)
    {
	assert(!isnan(ref[i]));
	assert(!isnan(res[i]));

	const double err = ref[i] - res[i];
	const double maxval = fmax(fabs(res[i]), fabs(ref[i]));
	const double relerr = err / fmax(1e-6, maxval);

	if (fabs(relerr) >= tol && fabs(err) >= tol)
	    printf("%d: %e ref: %e -> %e %e\n", i, res[i], ref[i], err, relerr);

	assert(fabs(relerr) < tol || fabs(err) < tol);

	linf_e = fmax(linf_e, fabs(err));
	l1_e += fabs(err);

	linf_ref = fmax(linf_ref, fabs(ref[i]));
	l1_ref += fabs(ref[i]);
    }

    const double linf_erel = linf_e / linf_ref;
    const double l1_erel = l1_e / l1_ref;

    printf("l-infinity errors: %.03e (absolute) %.03e (relative)\n", linf_e, linf_erel);
    printf("       l-1 errors: %.03e (absolute) %.03e (relative)\n", l1_e, l1_erel);
}

unsigned long long rdtsc(void)
{
    unsigned hi, lo;
    __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
    return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}

float rdata[WN], idata[WN];

fftwf_complex ic[WN], sol[WN], test[WN];

int main()
{
    _MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);

    int c, i;
    for(c = 0; c < W; ++c)
	for(i = 0; i < N; ++i)
	{
	    ic[c + W * i][0] = i + 0.5 - NH;
	    ic[c + W * i][1] = NH - fabsf(0.5 + i - NH);
	}

    int n = N;
    int * iembed = &n, *oembed = &n;
    fftwf_plan plan = fftwf_plan_many_dft(1, &n, W,
					  ic, iembed, W, 1, 
					  sol, oembed, W, 1, DIR, FFTW_ESTIMATE);

    fftwf_execute(plan);

    fftwf_destroy_plan(plan);

    for(c = 0; c < W; ++c)
	for(i = 0; i < N; ++i)
	{
	    rdata[c + W * i] = ic[c + W * i][0];
	    idata[c + W * i] = ic[c + W * i][1];
	}

    _KERNEL_(rdata, idata);

    for(c = 0; c < W; ++c)
	for(i = 0; i < N; ++i)
	{
	    test[c + W * i][0] = rdata[c + W * i];
	    test[c + W * i][1] = idata[c + W * i];
	}

    checkacc((float *)sol, (float *)test, WN * 2, 1e-4);

    const uint64_t t0 = rdtsc();

    for(int i = 0; i < ntimes; ++i)
	_KERNEL_(rdata, idata);

    const uint64_t t1 = rdtsc();

    printf("THROUGHPUT:  %.3f B/C\n"
	   "PERFORMANCE: %.3f F/C\n"
	   "%% of PEAK :  %.2f %%\n",
	   ntimes * sizeof(rdata) * 2 / (double)(t1 - t0),
	   ntimes * (size_t)_FLOPS_ * W / (double)(t1 - t0),
	   ntimes * (size_t)_FLOPS_ * W / (double)(t1 - t0) * 100. / 32);

    return EXIT_SUCCESS;
}
