This subfolder generates and deploys object files with one of the following signatures:

```
void r2c8(float * inout);
```
or
```
void r2c1(float * inout);
```


Remarks:
* The input size of the transforms is controlled by the makefile variable "rsize".
* The transforms are performed in place.
* For a transform of size "rsize", "rsize / 2 + 1" coefficients will be computed. The first and last coefficients will be real-valued only, the other coefficients are complex numbers.
* The output is organized as follows:
  - inout[0] is the first coefficient, and doesn not have an imaginary component
  - inout[1 : rsize/2 - 1] are the real components of the second to second last coefficients
  - inout[rsize/2 + 1 : rsize] are the imaginary components of the second to second last coefficients
  - inout[rsize/2] represents the last coefficient, and it does not have an imaginary counterpart.
* The number 1 and 8 in the signatures represent the number of independent 1D transforms that the function performs and it is controlled with the makefile variable "width".
* In the deployed object file, the function name changes according to the makefile variable "name".
* The target instruction set is AVX2.
* The data passed to r2c8 must have the AoS8 format.
